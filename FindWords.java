/*
 * FindWords.java
 * used to search through a given matrix for a set of given words.
 * @author Tom Batchlear
 */

public class FindWords {
    
    private String[][] wordsToFind;
    private String[][] puzzleMatrix;
    private String startingPosition;
    private String endingPosition;
    
    public FindWords(){
        
    } // end FindWords constructor
    
    void setWordsToFind(String[][] wordsToFind){
        this.wordsToFind = wordsToFind;
    } // end setWordsToFind
    
    void setMatrix(String[][] puzzleMatrix){
        this.puzzleMatrix = puzzleMatrix;
    } // end setMatrix()
    
    String[][] getMatrix(){
        return this.puzzleMatrix;
    } // end getMatrix()
    
    String[][] getFoundWords(){
        return wordsToFind;
    } //end getFoundWords()
    
    // locates the word by locating the first letter of the word and then
    // calling the traverseMatrix method from the current pointer location
    void findWords(){
        if (this.wordsToFind == null){
            System.out.println("The word matrix has not been loaded. Please choose a file with a matrix and attempt to search again");
        } else {
            int position = 0;
            String[] words = new String[wordsToFind.length];
            
            for (int i = 0; i < words.length; i++){
                words[i] = wordsToFind[i][0];
            }
            
            for (String word : words){
                char[] temp = word.toCharArray();
                
                for (int i = 0; i < puzzleMatrix.length; i++){
                    for (int j = 0; j < puzzleMatrix[0].length; j++){
                        if (puzzleMatrix[i][j].equals(Character.toString(temp[0])) && endingPosition == null){
                            startingPosition = i + ":" + j;
                            traverseMatrix(word, position, i, j);
                            
                            wordsToFind[position][1] = startingPosition;
                            wordsToFind[position][2] = endingPosition;
                        }
                    }
                }
                endingPosition = null;
                position++;
            }
            
        }
    } // end findWords()
    
    // check each direction for the word
    void traverseMatrix(String word, int position, int posX, int posY){
        
        int movementCount = 0;
        
        if (canMove("North", word, posX, posY)){
            moveNorth(word, movementCount, posX, posY);
        }
        
        if (canMove("North", word, posX, posY) && canMove("East", word, posX, posY)){
            moveNorthEast(word, movementCount, posX, posY);
        }
        
        if (canMove("East", word, posX, posY)){
            moveEast(word, movementCount, posX, posY);
        }
        
        if (canMove("East", word, posX, posY) && canMove("South", word, posX, posY)){
            moveSouthEast(word, movementCount, posX, posY);
        }
        
        if (canMove("South", word, posX, posY)){
            moveSouth(word, movementCount, posX, posY);
        }
        
        if (canMove("South", word, posX, posY) && canMove("West", word, posX, posY)){
            moveSouthWest(word, movementCount, posX, posY);
        }
        
        if (canMove("West", word, posX, posY)){
            moveWest(word, movementCount, posX, posY);
        }
        
        if (canMove("North", word, posX, posY) && canMove("West", word, posX, posY)){
            moveNorthWest(word, movementCount, posX, posY);
        }
    } // end traverseMatrix()
    
    // check that traversing matrix doesn't exceed index bounds
    boolean canMove(String direction, String word, int posX, int posY){
        
        switch (direction) {
            case "North":
                if (posX - word.length()+1 < 0){
                    return false;
                }   break;
            case "South":
                if (posX + word.length() > puzzleMatrix.length){
                    return false;
                }   break;
            case "East":
                if (posY + word.length() > puzzleMatrix.length){
                    return false;
                }   break;
            case "West":
                if (posY - word.length()+1 < 0){
                    return false;
                }   break;
            default:
                break;
        }
        return true;
    } // end canMove()
    
    // Recursively Move North until the word is found, the edge is found, or no match is found
    private void moveNorth(String word, int move, int x, int y){
        
        move = move + 1;
        
        int newX = x-1;
        int newY = y;

        if (move == word.length()) {
            endingPosition = x + ":" + y;
        } else if (puzzleMatrix[newX][newY].equals(Character.toString(word.charAt(move)))){
            moveNorth(word, move, newX, newY);
        } 
        
        
    } // end moveNorth()
    
    // Recursively Move NorthEast until the word is found, the edge is found, or no match is found
    private void moveNorthEast(String word, int move, int x, int y){
        
        move = move + 1;
        
        int newX = x-1;
        int newY = y+1;
         
        
        if (move == word.length()) {
            endingPosition = x + ":" + y;
        } else if (puzzleMatrix[newX][newY].equals(Character.toString(word.charAt(move)))){
            moveNorthEast(word, move, newX, newY);
        } 

    } // end moveNorthEast()
    
    // Recursively Move East until the word is found, the edge is found, or no match is found
    private void moveEast(String word, int move, int x, int y){
        
        move = move + 1;
        
        int newX = x;
        int newY = y+1;
        
        if (move == word.length()-1) {
            endingPosition = newX + ":" + newY;
        } else if (move < word.length() && puzzleMatrix[newX][newY].equals(Character.toString(word.charAt(move)))){
            moveEast(word, move, newX, newY);
        } 

    } // end moveEast()
    
    // Recursively Move SouthEast until the word is found, the edge is found, or no match is found
    private void moveSouthEast(String word, int move, int x, int y){
        
        move = move + 1;
        
        int newX = x+1;
        int newY = y+1;
        
        if (move == word.length()-1) {
            endingPosition = newX + ":" + newY;
        } else if (move != word.length() && puzzleMatrix[newX][newY].equals(Character.toString(word.charAt(move)))){
            moveSouthEast(word, move, newX, newY);
        } 

    }  // end moveSouthEast()
    
    // Recursively Move South until the word is found, the edge is found, or no match is found
    private void moveSouth(String word, int move, int x, int y){
        move = move + 1;
        int newX = x+1;
        int newY = y; 
        
        if (move == word.length()) {
            endingPosition = x + ":" + y;
        } else if (move != word.length() && move >= 0 && puzzleMatrix[newX][newY].equals(Character.toString(word.charAt(move)))){
            moveSouth(word, move, newX, newY);
        } 

    } // end moveSouth()
    
    // Recursively Move SouthWest until the word is found, the edge is found, or no match is found
    private void moveSouthWest(String word, int move, int x, int y){
        
        move = move + 1;
        
        int newX = x+1;
        int newY = y-1;
        
        if (move == word.length()) {
            endingPosition = x + ":" + y;
        } else if (move < word.length() && puzzleMatrix[newX][newY].equals(Character.toString(word.charAt(move)))){
            moveSouthWest(word, move, newX, newY);
        } 

    } //end moveSouthWest()
    
    // Recursively Move West until the word is found, the edge is found, or no match is found
    private void moveWest(String word, int move, int x, int y){
        move = move + 1;

        int newX = x;
        int newY = y-1;
        
        if (move == word.length()) {
            endingPosition = newX + ":" + y;
        } else if (puzzleMatrix[newX][newY].equals(Character.toString(word.charAt(move)))){
            moveWest(word, move, newX, newY);
        } 

    } // end moveWest()
    
    // Recursively Move NorthWest until the word is found, the edge is found, or no match is found
    private void moveNorthWest(String word, int move, int x, int y){
        move = move + 1;

        int newX = x-1;
        int newY = y-1;
        
        if (move == word.length()) {
            endingPosition = x + ":" + y;
        } else if (puzzleMatrix[newX][newY].equals(Character.toString(word.charAt(move)))){
            moveNorthWest(word, move, newX, newY);
        } 

    } // end moveNorthWest ()
    
} // end FindWords

