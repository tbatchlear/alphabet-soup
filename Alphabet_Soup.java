/*
 *Alphabet Soup project for Enlight Programming Challenge
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;


public class Alphabet_Soup {
    
    BuildPuzzle puzzle = new BuildPuzzle();
    
    FindWords findWords = new FindWords();

    public static void main(String[] args) throws FileNotFoundException {
        
        Alphabet_Soup AP = new Alphabet_Soup();
        
        if (AP.selectFile()){
        
            AP.searchMatrix();
        
            // display matrix
            for (int i = 0; i < AP.findWords.getMatrix().length; i++){
                    System.out.println(Arrays.toString(AP.findWords.getMatrix()[i]));
            }
        
            // display found words and their starting and ending points
            for (int i = 0; i < AP.findWords.getFoundWords().length; i++){
                System.out.println(Arrays.toString(AP.findWords.getFoundWords()[i]));
            }
        }
    } // end main()
  
    boolean selectFile() throws FileNotFoundException {
        JFileChooser jfc = new JFileChooser();
        File workingDirectory = new File(System.getProperty("user.dir"));
        jfc.setCurrentDirectory(workingDirectory);
        jfc.setAcceptAllFileFilterUsed(false);
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Text Files", "txt");
        jfc.addChoosableFileFilter(filter);
        int returnValue = jfc.showOpenDialog(null);
        try {
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                File selectedFile = jfc.getSelectedFile();
                if (puzzle.process(selectedFile)){
                    return true;
                }
            }
        } catch (NullPointerException e) {
        }
        return false;
    }//end selectFile()
    
    public void searchMatrix(){
        findWords.setWordsToFind(puzzle.getWordsToFind());
        findWords.setMatrix(puzzle.getPuzzle());
        
        findWords.findWords();
    } // end searchMatrix()
    
} // end Alphabet_Soup

