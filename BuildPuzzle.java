/*
 * BuildPuzzle.java
 * Reads a given file and build a matrix
 * identifies words to find within the matrix
 * @author Tom Batchlear
 */

import java.io.*;
import java.util.*;

public class BuildPuzzle {
    
    private List<String> puzzleList = new ArrayList<String>();
    private String[][] puzzleMatrix;
    private String[][] wordsToFind;
    
    public BuildPuzzle(){
    } // end BuildPuzzle constructor
    
    public String[][] getPuzzle(){
        return puzzleMatrix;
    } // end getPuzzle
    
    public String[][] getWordsToFind(){
        return wordsToFind;
    } // end getWordsToFind
    
    // processes the file selected by the user
    boolean process(File selectedFile){
        
        try (Scanner sc1 = new Scanner(selectedFile)){
            while (sc1.hasNextLine()) {
                puzzleList.add(sc1.nextLine());
            }
            sc1.close();
        } catch (FileNotFoundException e) {
            System.out.println("File Not Found: " + e.getMessage());
        } catch (Exception e) {
            System.out.println("Error occured while processing file: " + e.getMessage());
        } // end try...catch for Scanner
        
        
        String[] arr = puzzleList.toArray(new String[0]);
        
        if (!checkFile(arr)){
            System.out.println("incorrect file type.  Please try again");
            return false;
        } else {
        
            for (int i = 0; i < arr.length; i++){
                arr[i] = arr[i].replaceAll("\\s", "");
            }

            processMatrixSize(arr);

            buildPuzzle(arr, puzzleMatrix);

            processWords(arr);
        
            return true;
        }
        
    } // end process()
    
    // check that file is compatible with program
    boolean checkFile(String[] arr){
        int end = arr[0].length();
        int front = 0;
        
        if (arr[0].length()%2 == 0){
            return false;
        }
        
        if (arr[0].charAt(arr[0].length()/2) != "x".charAt(0)){
            return false;
        }
        
        return true;
    }
    
    private void processMatrixSize(String[] arr){
        char[] matrixArraySize = arr[0].toCharArray();
        
        puzzleMatrix = new String[Character.getNumericValue(matrixArraySize[0])][Character.getNumericValue(matrixArraySize[2])];

    }// end processMatrixSize()
    
    private void buildPuzzle(String[] arr, String[][] puzzleMatrix){

        for (int i = 1; i <= puzzleMatrix.length; i++){
            for (int j = 0; j < puzzleMatrix.length; j++){
                puzzleMatrix[i-1][j] = String.valueOf(arr[i].charAt(j));
            }
        }
        
    } // end buildPuzzle()
    
    private void processWords(String[] arr){

        wordsToFind = new String[arr.length - puzzleMatrix.length - 1][3];
        int temp = 0;
        
        for (int i = puzzleMatrix.length + 1; i < arr.length; i++){
            wordsToFind[temp][0] = arr[i];
            temp++;
        }
    } // end processWords()
}


